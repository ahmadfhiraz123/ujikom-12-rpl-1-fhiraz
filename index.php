<?php
    session_start();
    include 'login/koneksi.php';
    if (@$_SESSION['id_level'] == '1') 
    {
        header("location: login/entry/admin/");
    }
    elseif (@$_SESSION['id_level'] == '2') 
    {
        header("location: login/entry/operator/");
    }
    elseif (@$_SESSION['id_level'] == '3') 
    {
        header("location: login/entry/peminjam/");
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="login/assets/images/favicon.png">
    <title>ISP-SKANIC 2019</title>
    <!-- Custom CSS -->
    <link href="login/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="login/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="login/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="login/assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="login/assets/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="login/assets/extra-libs/prism/prism.css">
    <!-- Custom CSS -->
    <link href="login/dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" style="background: #333d54;">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" style="background: #333d54;">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index" style="background: #333d54;">
                        <!-- Logo icon -->
                        <b class="logo-icon">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="login/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="login/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src="login/assets/images/text-2.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo text -->
                            <img src="login/assets/images/text-2.1.png" class="light-logo" alt="homepage" />
                        </span>
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" style="background: #333d54;">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="sl-icon-menu font-20"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <img src="login/assets/images/icon/login.png" alt="user" class="rounded-circle" width="34">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <span class="with-arrow">
                                    <span class="bg-primary"></span>
                                </span>
                                <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                                    <div class="">
                                        <img src="login/assets/images/icon/login-1.png" alt="user" class="img-circle" width="60">
                                    </div>
                                    <div class="m-l-10">
                                        <h4 class="m-b-0">Halaman Login</h4>
                                        <p class=" m-b-0">Untuk semua pengguna</p>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="login/">
                                    <i class="fas fa-sign-in-alt m-r-5 m-l-5"></i> Login</a>
                                <div class="dropdown-divider"></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Home</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Welcome</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <div class="row">
            <div class="col-md-12 col-xs-12">
            <div class="card">
            <div class="card-body">
            <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators3" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators3" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators3" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="img-fluid" src="login/assets/images/icon/gallery.jpg" width="100%" style="height: 350px;" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3 class="text-white">Selamat Datang</h3>
                            <p>Di website ISP Skanic</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-fluid" src="login/assets/images/icon/gallery1.jpg" width="100%" style="height: 350px;" alt="Second slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3 class="text-white">Inventarisir</h3>
                            <p>Ada banyak sekali inventaris di SMKN 1 Ciomas</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="img-fluid" src="login/assets/images/icon/gallery2.jpg" width="100%" style="height: 350px;" alt="Third slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h3 class="text-white">Pinjam Barang</h3>
                            <p>Anda bisa pinjam barang di website ini, tetapi Anda harus Login terlebih dahulu</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12 col-xs-12">
            <div class="card">
            <div class="card-body">
                <h4 class="card-subtitle">
                    Daftar Inventaris 
                </h4>
                <div class="row el-element-overlay">
                    <?php 
                        include "login/koneksi.php";
                        // Cek apakah terdapat data page pada URL
                        $page = (isset($_GET['page']))? $_GET['page'] : 1;
                            
                        $limit = 8; // Jumlah data per halamannya
                            
                        // Untuk menentukan dari data ke berapa yang akan ditampilkan pada tabel yang ada di database
                        $limit_start = ($page - 1) * $limit;

                        $sql = mysqli_query($conn, "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas WHERE jumlah>=1 ORDER BY id_inventaris DESC LIMIT ".$limit_start.",".$limit);
                         while ($data=mysqli_fetch_array($sql)) {
                    ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1"> <img src="login/assets/images/icon/inventaris-1.png" alt="user" />
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="login/assets/images/icon/inventaris-1.png"><i class="sl-icon-magnifier"></i></a></li>
                                            <li class="el-item"><button class="btn default btn-outline el-link" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris'];?>" ><i class="sl-icon-eye"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="m-b-0"><?php echo $data['nama']; ?></h4> <span class="text-muted"><?php echo $data['nama_jenis']; ?></span>
                                </div>
                            </div>
                            <div id="detail-inventaris<?php echo $data['id_inventaris']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Detail Inventaris</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <div class="modal-body">
                                        <form class="form-horizontal">
                                            <div class="form-body">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Kode Inventaris :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['kode_inventaris']; ?> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Nama Barang :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['nama']; ?> </p>
                                                                </div>
                                                             </div>
                                                        </div>
                                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Jenis :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['nama_jenis']; ?> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Tanggal Register :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo date('d F Y', strtotime($data['tanggal_register'])); ?> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Jumlah :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['jumlah']; ?>  </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Ruang :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['nama_ruang']; ?>  </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Petugas :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['nama_petugas']; ?>  </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Kondisi :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['kondisi']; ?>  </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Spesifikasi :</label>
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static"> <?php echo $data['spesifikasi']; ?>  </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Keterangan :</label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static"> <?php echo $data['keterangan_barang']; ?>  </p>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="control-label text-right col-md-6">Sumber :</label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static"> <?php echo $data['sumber']; ?> </p>
                                                            </div>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12">
                                                             <div class="form-group row">
                                                             <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                                 <label class="control-label text-right col-md-6">Status :</label>
                                                                 <div class="col-md-6">
                                                                     <p class="form-control-static">
                                                                     <label class="label label-table label-danger">Tidak Aktif</label></p>
                                                                 </div>
                                                                 <?php }else{ ?>
                                                                 <label class="control-label text-right col-md-6">Status :</label>
                                                                 <div class="col-md-6">
                                                                     <p class="form-control-static"> <label class="label label-table label-success">Aktif</label></p>
                                                                 </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                         <!--/span-->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div> 
                        </div>
                    </div>
                    <?php } ?>
                </div> 
            </div> 
            <div class="row">
                    <div class="col-md-12">
                    <ul class="pagination justify-content-center">
                        <!-- Start Link First -->
                        <?php
                            if($page == 1){ // Jika page adalah page ke 1, maka disable link PREV
                            ?>
                                <li class="page-item disabled"><a class="page-link" href="#">First</a></li> <!-- Link disabled -->
                            <?php
                            }else{ // Jika page bukan page ke 1
                                $link_prev = ($page > 1)? $page - 1 : 1;
                            ?>
                                <li class="page-item"><a class="page-link" href="index?page=1">First</a></li>
                            <?php
                            }
                        ?>
                        <!-- End LinkFirst -->
                        
                        <!-- START LINK NUMBER -->
                        <?php
                            // Buat query untuk menghitung semua jumlah data
                            $sql2 = mysqli_query($conn, "SELECT COUNT(*) AS jumlah FROM tb_inventaris ");
                            $get_jumlah = mysqli_fetch_array($sql2);
                                
                            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamannya
                            $jumlah_number = 2; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
                            $start_number = ($page > $jumlah_number)? $page - $jumlah_number : 1; // Untuk awal link number
                            $end_number = ($page < ($jumlah_page - $jumlah_number))? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number
                                
                            for($i = $start_number; $i <= $end_number; $i++){
                                $link_active = ($page == $i)? ' class="page-item active"' : '';
                            ?>
                                <li<?php echo $link_active; ?>><a class="page-link" href="index?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                            <?php
                            }
                        ?>
                        <!-- END LINK SUMBER -->

                        <!-- START LINK LAST -->
                        <?php
                            // Jika page sama dengan jumlah page, maka disable link NEXT nya
                            // Artinya page tersebut adalah page terakhir 
                            if($page == $jumlah_page){ // Jika page terakhir
                            ?>
                                <li class="page-item disabled"><a class="page-link" href="#">Last</a></li>
                            <?php
                            }else{ // Jika Bukan page terakhir
                                $link_next = ($page < $jumlah_page)? $page + 1 : $jumlah_page;
                            ?>
                                <li class="page-item"><a class="page-link" href="index?page=<?php echo $jumlah_page; ?>">Last</a></li> 
                            <?php
                            }
                        ?>
                        <!-- END LINK LAST -->
                    </ul>
                    </div>
                    </div>
                    </div>
            </div>
            </div>
            </div> 
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="login/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="login/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="login/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="login/dist/js/app.min.js"></script>
    <script src="login/dist/js/app.init.horizontal.js"></script>
    <script src="login/dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="login/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="login/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="login/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="login/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="login/dist/js/custom.min.js"></script>
    <script src="login/assets/extra-libs/prism/prism.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="login/assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="login/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="login/assets/extra-libs/c3/d3.min.js"></script>
    <script src="login/assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="login/assets/libs/raphael/raphael.min.js"></script>
    <script src="login/assets/libs/morris.js/morris.min.js"></script>
    <!--This page JavaScript -->
    <script src="login/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="login/assets/libs/magnific-popup/meg.init.js"></script>

    <script src="login/dist/js/pages/dashboards/dashboard1.js"></script>

</body>

</html>