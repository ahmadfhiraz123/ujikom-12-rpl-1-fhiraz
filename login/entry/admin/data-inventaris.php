<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Administrator</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="icon-Box-withFolders"></i> Data Inventaris</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="data-inventaris">Inventaris</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Inventaris</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-subtitle">
                                    <button type="button" class="btn waves-effect waves-light btn-dark" data-toggle="modal" data-target="#tambah-inventaris" style="background: #333d54;"><i class="fa fa-plus"></i> Tambah Barang</button>
                                                <div id="tambah-inventaris" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-xl">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Inventaris</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="proses/simpan-inventaris" method="post" class="form-horizontal form-material needs-validation" novalidate>
                                                                  <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Nama Barang :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="nama" maxlength="30" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kondisi :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="kondisi" required="">
                                                                            <option value="" disabled selected>Pilih Kondisi Barang</option>        
                                                                            <option value="Baik">Baik</option>
                                                                            <option value="Rusak">Rusak</option>
                                                                            <option value="Rusak Parah">Rusak Parah</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Spesifikasi :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="spesifikasi" maxlength="30" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Keterangan :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="keterangan_barang" required="">
                                                                            <option value="" disabled selected>Pilih Keterangan</option>        
                                                                            <option value="Tersedia">Tersedia</option>
                                                                            <option value="Tidak Tersedia">Tidak Tersedia</option>
                                                                            </select>
                                                                        </div>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Jumlah :</label>
                                                                            <input type="number" class="form-control" id="recipient-name1" name="jumlah" min="1" maxlength="30" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Jenis :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_jenis" required="">
                                                                            <option value="" disabled selected>Pilih Jenis Barang</option>
                                                                            <?php
                                                                                include "../../koneksi.php";
                                                                                $query = mysqli_query($conn, "SELECT * from tb_jenis");
                                                                                while ($data=mysqli_fetch_array($query)) {
                                                                                ?>
                                                                                <option value="<?php echo $data['id_jenis']; ?>"><?php echo $data['nama_jenis']; ?></option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Ruang :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_ruang" required="">
                                                                            <option value="" disabled selected>Pilih Ruangan</option>
                                                                            <?php
                                                                                include "../../koneksi.php";
                                                                                $query = mysqli_query($conn, "SELECT * from tb_ruang");
                                                                                while ($data=mysqli_fetch_array($query)) {
                                                                                ?>
                                                                                <option value="<?php echo $data['id_ruang']; ?>"><?php echo $data['nama_ruang']; ?></option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20" hidden="">
                                                                            <label for="recipient-name" class="control-label">Petugas :</label>
                                                                            <?php
                                                                            include "../../koneksi.php";
                                                                            $nama_petugas = $_SESSION['username'];
                                                                            $query = mysqli_query($conn, "SELECT * from tb_petugas WHERE username = '$nama_petugas'");
                                                                            $data=mysqli_fetch_array($query);
                                                                            ?>
                                                                            <input name="id_petugas" value="<?php echo $data['id_petugas'];?>" type="text" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Sumber :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="sumber" required="">
                                                                            <option value="" disabled selected>Pilih Sumber</option>        
                                                                            <option value="Pemerintah">Pemerintah</option>
                                                                            <option value="Sekolah">Sekolah</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kode Inventaris :</label>
                                                                            <?php
                                                                            include '../../koneksi.php';
                                                                            //mencari kode brng dgn nilai paling besar
                                                                            $query = "SELECT max(kode_inventaris) AS maxKode FROM tb_inventaris";
                                                                            $hasil = mysqli_query($conn, $query);
                                                                            $data = mysqli_fetch_array($hasil);
                                                                            $kode_inventaris = $data['maxKode'];

                                                                            //mengambil angka atau bilangan dalam kode anggota terbesar
                                                                            //dengan cara mengambil substring mulai dari karakter ke-1 diambil 6 karakter
                                                                            //misal 'BRG001' akan diambil '001'
                                                                            //setelah substring bilangan diambil lantas dicasting menjadi integer
                                                                            $noUrut = (int) substr($kode_inventaris, 3, 3);

                                                                            //bilangan ini ditambah 1 utk menentukan nomor urut berikutnya
                                                                            $noUrut++;

                                                                            //membentuk kode baru
                                                                            //perintah sprintf("%03s", $noUrut); digunakan utk memformat string sebanyak 3 karakter
                                                                            //misal sprintf("%03s", 12); maka akan dihasilkan '012'
                                                                            $char = "BRG";
                                                                            $kode_inventaris = $char . sprintf("%03s", $noUrut);
                                                                            {?>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="kode_inventaris" maxlength="30" required="" value="<?php echo $kode_inventaris; ?>" hidden>
                                                                            <?php echo $kode_inventaris; ?>
                                                                            <?php }?>
                                                                        </div>
                                                                    </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-dark waves-effect" value="Action" name="action" style="background: #333d54;">Simpan</button>
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                </h6>
                                <div class="table-responsive">
                                    <table id="file_export-1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Kode Inventaris</th>
                                                <th>Nama Barang</th>
                                                <th>Jenis</th>
                                                <th>Kondisi</th>
                                                <th>Sumber</th>
                                                <th>Status</th>
                                                <th>Keterangan Stok</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                              include "../../koneksi.php";
                                              $no=1;
                                              $query = mysqli_query($conn, "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas ORDER BY id_inventaris DESC");
                                              while ($data=mysqli_fetch_array($query)) {
                                              ?>
                                            <tr class="text-center">
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $data['kode_inventaris']; ?></td>
                                                <td><?php echo $data['nama']; ?></td>
                                                <td><?php echo $data['nama_jenis']; ?></td>
                                                <td><?php echo $data['kondisi']; ?></td>
                                                <td><?php echo $data['sumber']; ?></td>
                                                <td>
                                                <?php if ($data['status'] == 'Tidak Aktif') {
                                                    echo "<a type='button' data-toggle='tooltip' data-placement='top' title='Rubah Status' class='btn waves-effect waves-light btn-danger card-hover status-link' href=\"proses/proses-status?id_inventaris=$data[id_inventaris]&status=Aktif\">Tidak Aktif</a>";
                                                    }else{
                                                    echo "<a type='button' data-toggle='tooltip' data-placement='top' title='Rubah Status' class='btn waves-effect waves-light btn-success card-hover status-link' href=\"proses/proses-status?id_inventaris=$data[id_inventaris]&status=Tidak Aktif\">Aktif</a>";
                                                    }   
                                                    ?> 
                                                </td>
                                                 <td>
                                                     <?php if ($data['jumlah'] >= 1) {?>
                                                     <label class="label label-success" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Tersedia">Stok Tersedia</label>
                                                     <?php }else{ ?>
                                                     <label class="label label-danger" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Kosong ">Stok Kosong</label>
                                                     <?php } ?>
                                                 </td>
                                                <td>
                                                <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                    <a class="btn waves-effect waves-light btn-danger card-hover delete-link" href="proses/hapus-inventaris?id_inventaris=<?php echo $data['id_inventaris']; ?>"><i class="fa fa-trash"></i></a>
                                                <?php }else{ ?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                    <button type="button" class="btn waves-effect waves-light btn-info card-hover"><i class="fa fa-edit" data-toggle="modal" data-target="#edit-inventaris<?php echo $data['id_inventaris'];?>"></i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                                <div id="edit-inventaris<?php echo $data['id_inventaris'];?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-xl">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Edit Data Inventaris</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="proses/update-inventaris" method="post" class="form-horizontal form-material">
                                                                  <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                        <input type="hidden" class="form-control" id="recipient-name1" name="id_inventaris" maxlength="30" required="" value="<?php echo $data['id_inventaris']; ?>">
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Nama Barang :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="nama" maxlength="30" required="" value="<?php echo $data['nama']; ?>">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kondisi :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="kondisi" required="">
                                                                            <option value="" disabled selected>Pilih Kondisi Barang</option>        
                                                                            <option value="Baik" <?php if($data['kondisi']== 'Baik') {echo 'selected="selected"';} ?>>Baik</option>
                                                                            <option value="Rusak" <?php if($data['kondisi']== 'Rusak') {echo 'selected="selected"';} ?>>Rusak</option>
                                                                            <option value="Rusak Parah" <?php if($data['kondisi']== 'Rusak Parah') {echo 'selected="selected"';} ?>>Rusak Parah</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Spesifikasi :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="spesifikasi" maxlength="30" required="" value="<?php echo $data['spesifikasi']; ?>">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Keterangan :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="keterangan_barang" required="">
                                                                            <option value="" disabled selected>Pilih Keterangan</option>        
                                                                            <option value="Tersedia" <?php if($data['keterangan_barang']== 'Tersedia') {echo 'selected="selected"';} ?>>Tersedia</option>
                                                                            <option value="Tidak Tersedia" <?php if($data['keterangan_barang']== 'Tidak Tersedia') {echo 'selected="selected"';} ?>>Tidak Tersedia</option>
                                                                            </select>
                                                                        </div>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Jumlah :</label>
                                                                            <input type="number" class="form-control" id="recipient-name1" name="jumlah" maxlength="30" min="1" required="" value="<?php echo $data['jumlah']; ?>">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Jenis :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_jenis" required="">
                                                                            <option value="" disabled selected>Pilih Jenis Barang</option>
                                                                            <?php
                                                                                include "../../koneksi.php";
                                                                                $query1 = mysqli_query($conn, "SELECT * from tb_jenis");
                                                                                while ($data1=mysqli_fetch_array($query1)) {
                                                                                ?>
                                                                                <option value="<?php echo $data1['id_jenis']; ?>" <?php if($data['id_jenis']==$data1['id_jenis']) {echo 'selected="selected"';} ?>><?php echo $data1['nama_jenis']; ?></option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Ruang :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_ruang" required="">
                                                                            <option value="" disabled selected>Pilih Ruangan</option>
                                                                            <?php
                                                                                include "../../koneksi.php";
                                                                                $query1 = mysqli_query($conn, "SELECT * from tb_ruang");
                                                                                while ($data1=mysqli_fetch_array($query1)) {
                                                                                ?>
                                                                                <option value="<?php echo $data1['id_ruang']; ?>" <?php if($data['id_ruang']==$data1['id_ruang']) {echo 'selected="selected"';} ?>><?php echo $data1['nama_ruang']; ?></option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20" hidden="">
                                                                            <label for="recipient-name" class="control-label">Petugas :</label>
                                                                            <?php
                                                                            include "../../koneksi.php";
                                                                            $nama_petugas = $_SESSION['username'];
                                                                            $query1 = mysqli_query($conn, "SELECT * from tb_petugas WHERE username = '$nama_petugas'");
                                                                            $data1=mysqli_fetch_array($query1);
                                                                            ?>
                                                                            <input name="id_petugas" value="<?php echo $data1['id_petugas'];?>" type="text" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Sumber :</label>
                                                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="sumber" required="">
                                                                            <option value="" disabled selected>Pilih Sumber</option>        
                                                                            <option value="Pemerintah" <?php if($data['sumber']== 'Pemerintah') {echo 'selected="selected"';} ?>>Pemerintah</option>
                                                                            <option value="Sekolah" <?php if($data['sumber']== 'Sekolah') {echo 'selected="selected"';} ?>>Sekolah</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kode Inventaris :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="kode_inventaris" maxlength="30" required="" value="<?php echo $data['kode_inventaris']; ?>" hidden>
                                                                            <?php echo $data['kode_inventaris']; ?>
                                                                        </div>
                                                                    </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-dark waves-effect" value="Update" name="update" style="background: #333d54;">Update</button>
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <div id="detail-inventaris<?php echo $data['id_inventaris']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Data Inventaris</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <form class="form-horizontal">
                                                            <div class="form-body">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kode Inventaris :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kode_inventaris']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Nama Barang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jenis :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_jenis']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Tanggal Register :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo date('d F Y', strtotime($data['tanggal_register'])); ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jumlah :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['jumlah']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Ruang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_ruang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!--/row-->
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Petugas :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_petugas']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kondisi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kondisi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Spesifikasi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['spesifikasi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Keterangan :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['keterangan_barang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Sumber :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['sumber']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                            <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static">
                                                                                    <label class="label label-table label-danger">Tidak Aktif</label></p>
                                                                                </div>
                                                                                <?php }else{ ?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <label class="label label-table label-success">Aktif</label></p>
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>  
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-1.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-2.init.js"></script>

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Menghapus?",   
                    text: "Kamu Akan Kehilangan Data Barang Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Hapus Data!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.status-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Yakin Ingin Merubah Status?",   
                    text: "Status Akan Berubah",   
                    type: "warning",   
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Rubah Status!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>
</html>