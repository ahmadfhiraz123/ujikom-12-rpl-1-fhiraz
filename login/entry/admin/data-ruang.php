<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Administrator</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="icon-Box-withFolders"></i> Data Ruang</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="data-ruang">Ruang</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Ruang</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <h6 class="card-subtitle">
                                    <button type="button" class="btn waves-effect waves-light btn-dark" data-toggle="modal" data-target="#tambah-ruang" style="background: #333d54;"><i class="fa fa-plus"></i> Tambah Ruang</button>
                                                <div id="tambah-ruang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Tambah Data Ruang</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="proses/simpan-ruang" method="post" class="form-horizontal form-material needs-validation" novalidate>
                                                                    <div class="form-group col-md-12">
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Nama Ruang :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="nama_ruang" maxlength="30" required="">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="message-text" class="control-label">Keterangan :</label>
                                                                            <textarea class="form-control" id="message-text1" rows="3" name="keterangan" maxlength="100" required=""></textarea>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kode Ruang :</label>
                                                                            <?php
                                                                            include '../../koneksi.php';
                                                                            //mencari kode brng dgn nilai paling besar
                                                                            $query = "SELECT max(kode_ruang) AS maxKode FROM tb_ruang";
                                                                            $hasil = mysqli_query($conn, $query);
                                                                            $data = mysqli_fetch_array($hasil);
                                                                            $kode_ruang = $data['maxKode'];

                                                                            //mengambil angka atau bilangan dalam kode anggota terbesar
                                                                            //dengan cara mengambil substring mulai dari karakter ke-1 diambil 6 karakter
                                                                            //misal 'BRG001' akan diambil '001'
                                                                            //setelah substring bilangan diambil lantas dicasting menjadi integer
                                                                            $noUrut = (int) substr($kode_ruang, 3, 3);

                                                                            //bilangan ini ditambah 1 utk menentukan nomor urut berikutnya
                                                                            $noUrut++;

                                                                            //membentuk kode baru
                                                                            //perintah sprintf("%03s", $noUrut); digunakan utk memformat string sebanyak 3 karakter
                                                                            //misal sprintf("%03s", 12); maka akan dihasilkan '012'
                                                                            $char = "R";
                                                                            $kode_ruang = $char . sprintf("%03s", $noUrut);
                                                                            {?>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="kode_ruang" maxlength="30" required="" value="<?php echo $kode_ruang; ?>" hidden>
                                                                            <?php echo $kode_ruang; ?>
                                                                            <?php }?>
                                                                        </div>
                                                                    </div>
                                                                  <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-dark waves-effect" value="Action" name="action" style="background: #333d54;">Simpan</button>
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                </h6>
                                <div class="table-responsive">
                                    <table id="file_export-1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Kode Ruang</th>
                                                <th>Nama Ruang</th>
                                                <th>Keterangan</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                              include "../../koneksi.php";
                                              $no=1;
                                              $query = mysqli_query($conn, "SELECT * FROM tb_ruang ORDER BY id_ruang DESC");
                                              while ($data=mysqli_fetch_array($query)) {
                                              ?>
                                            <tr class="text-center">
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $data['kode_ruang']; ?></td>
                                                <td><?php echo $data['nama_ruang']; ?></td>
                                                <td><?php echo $data['keterangan']; ?></td>
                                                <td>
                                                    <button type="button" class="btn waves-effect waves-light btn-info card-hover"><i class="fa fa-edit" data-toggle="modal" data-target="#edit-ruang<?php echo $data['id_ruang'];?>"></i></button>
                                                    <a class="btn waves-effect waves-light btn-danger card-hover delete-link" href="proses/hapus-ruang?id_ruang=<?php echo $data['id_ruang']; ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                                <div id="edit-ruang<?php echo $data['id_ruang'];?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel">Edit Data Ruang</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="proses/update-ruang" method="post" class="form-horizontal form-material">
                                                                    <div class="form-group col-md-12">
                                                                        <input type="hidden" class="form-control" id="recipient-name1" name="id_ruang" maxlength="30" required="" value="<?php echo $data['id_ruang']; ?>">
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Nama Ruang :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="nama_ruang" maxlength="30" required="" value="<?php echo $data['nama_ruang']; ?>">
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="message-text" class="control-label">Keterangan :</label>
                                                                            <textarea class="form-control" id="message-text1" rows="3" name="keterangan" maxlength="150" required=""><?php echo $data['keterangan']; ?></textarea>
                                                                        </div>
                                                                        <div class="col-md-12 m-b-20">
                                                                            <label for="recipient-name" class="control-label">Kode Ruang :</label>
                                                                            <input type="text" class="form-control" id="recipient-name1" name="kode_ruang" maxlength="30" required="" value="<?php echo $data['kode_ruang']; ?>" hidden>
                                                                            <?php echo $data['kode_ruang']; ?>
                                                                        </div>
                                                                      </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-dark waves-effect" value="Update" name="update" style="background: #333d54;">Update</button>
                                                                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-1.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-2.init.js"></script>

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>
    
    <script>
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Menghapus?",   
                    text: "Kamu Akan Kehilangan Data Ruang Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Hapus Data!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>

</html>