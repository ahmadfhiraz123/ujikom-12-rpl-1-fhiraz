<?php
include('../cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Administrator</title>
    <!-- Custom CSS -->
    <link href="../../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../../assets/images/big/auth-bg.jpg) no-repeat center center;">

<?php 
include "../../../koneksi.php";

if (isset($_GET['id_inventaris'])) 
{
   $id_inventaris = $_GET['id_inventaris'];
}else{
  die ("Error. Tidak Ada Data!");
}

mysqli_query($conn,"DELETE FROM tb_inventaris WHERE id_inventaris='$id_inventaris'")or die(mysqli_error());

echo "
  <script type='text/javascript'>
  setTimeout(function() {
                                    swal({
                                        title: 'Barang Berhasil Dihapus!',
                                        text: 'Data Barang Telah Tiada!',
                                        icon: 'success',
                                        type: 'success',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.location.replace('../data-inventaris');
                                  },1000);</script>";

?>
</div>   
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
   
</body>
</html>