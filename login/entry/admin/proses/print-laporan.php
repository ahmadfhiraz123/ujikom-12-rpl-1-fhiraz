<?php ob_start(); ?>
<html>
<head>
	<title>Cetak Laporan PDF</title>
	<style type="text/css">
    h3,
    p {
      line-height: 10px;
      margin-bottom: 0px;
    }
    table, th, td {
      width: auto;
      border: 1px solid black;
      border-collapse: collapse;
    }
    th {
      background: #20a8d8;
    }
    th,
    td {
      padding: 5px;
      text-align: center;
    }
  </style>
</head>
<body>
    <div style="display: block;">
    <div style="float: left; position: absolute;">
      <img style="width: 200px ; height: 150px; margin-top: 40px; margin-left: 5px;" src="../../../assets/images/icon/logo1.png">
    </div>
    <div style="text-align: center;">
      <h3>PEMERINTAH KABUPATEN BOGOR</h3>
      <h3>DINAS PENDIDIKAN</h3>
      <h3>SMK NEGERI 1 CIOMAS</h3>
      <P>TEKNOLOGI DAN REKAYASA</P>
      <P>TEKNOLOGI INFORMASI DAN KOMUNIKASI</P>
      <P>NSS : 401020229101 NSPN : 20254135</P>
      <P>JL. Raya Laladon Des. Laladon Kec. Ciomas Kab. Bogor Telp : (0251) 8631216 Kode Pos. 16610</P>
    </div>  
    <div style="float: right; position: absolute; right: 0;">
      <img style="width: 100px ; height: 120px; margin-top: 50px; margin-right: 50px;" src="../../../assets/images/icon/logo.png">
    </div>
  </div>
  
  <hr style="margin-top: 20px;">
  <div style="text-align: center; margin-bottom: 20px;">
    <h3>Data Barang</h3>
  </div>

	<?php
	// Load file koneksi.php
	include "../../../koneksi.php";

	if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
     $filter = $_GET['filter']; // Ambil data filder yang dipilih user

    if($filter == '1'){ // Jika filter nya 1 (per tanggal)
    $tgl = date('d F Y', strtotime($_GET['tanggal']));

    echo '<b>Data Inventaris Tanggal : '.$tgl.'</b><br /><br />';
    
    $query = "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas WHERE DATE(tanggal_register)='".$_GET['tanggal']."'"; // Tampilkan data tb_inventaris sesuai tanggal yang diinput oleh user pada filter
    	}else if($filter == '2'){ // Jika filter nya 2 (per bulan)
            $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

            echo '<b>Data Inventaris Bulan : '.$nama_bulan[$_GET['bulan']].' '.$_GET['tahun'].'</b><br /><br />';

            $query = "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas WHERE MONTH(tanggal_register)='".$_GET['bulan']."' AND YEAR(tanggal_register)='".$_GET['tahun']."'"; // Tampilkan data tb_inventaris sesuai bulan dan tahun yang diinput oleh user pada filter
        }else{ // Jika filter nya 3 (per tahun)
            echo '<b>Data Inventaris Tahun : '.$_GET['tahun'].'</b><br /><br />';

            $query = "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas WHERE YEAR(tanggal_register)='".$_GET['tahun']."'"; // Tampilkan data tb_inventaris sesuai tahun yang diinput oleh user pada filter
        }
        }else{ // Jika user tidak mengklik tombol tampilkan
            echo '<b>Semua Data Inventaris</b><br /><br />';
            
            $query = "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas ORDER BY tanggal_register"; // Tampilkan semua data tb_inventaris diurutkan berdasarkan tanggal
        }
        ?>
		<table width="100%" border="1px;" style="border-collapse: collapse;" align="center">
                <thead>
                    <tr class="text-center">
                         <th>No</th>
                         <th>Tanggal</th>
                         <th>Kode Inventaris</th>
                         <th>Barang</th>
                         <th>Kondisi</th>
                         <th>Spesifikasi</th>
                         <th>Jumlah</th>
                         <th>Jenis</th>
                         <th>Ruang</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
                        $row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql

                        if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
                            $no=1;
                            while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                            $tgl = date('d F Y', strtotime($data['tanggal_register'])); // Ubah format tanggal jadi dd-mm-yyyy

                            echo "<tr class='text-center'>";
                            echo "<td>".$no++."</td>";
                            echo "<td>".$tgl."</td>";
                            echo "<td>".$data['kode_inventaris']."</td>";
                            echo "<td>".$data['nama']."</td>";
                            echo "<td>".$data['kondisi']."</td>";
                            echo "<td>".$data['spesifikasi']."</td>";
                            echo "<td>".$data['jumlah']."</td>";
                            echo "<td>".$data['nama_jenis']."</td>";
                            echo "<td>".$data['nama_ruang']."</td>";
                            echo "</tr>";
                            }
                            }else{ // Jika data tidak ada
                                echo "<tr class='text-center'><td colspan='9'>Data tidak ada</td></tr>";
                                }
                            ?>
                        </tbody>
                    </table>
</body>
</html>
<?php
$html = ob_get_contents();
ob_end_clean();

require_once('../../../assets/plugin/html2pdf/html2pdf.class.php');
$pdf = new HTML2PDF('P','A4','en');
$pdf->WriteHTML($html);
$pdf->Output('Data Laporan Barang.pdf', 'D');
?>
