<?php
include('../cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Administrator</title>
    <!-- Custom CSS -->
    <link href="../../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(../../../assets/images/big/auth-bg.jpg) no-repeat center center;">

<?php 
include "../../../koneksi.php";

if (isset($_POST['action'])) 
{
  $username            = addslashes(strip_tags($_POST['username']));
  $password            = $_POST['password'];
  $konfirmasi_password = $_POST['konfirmasi_password'];
  $nama_petugas        = $_POST['nama_petugas'];
  $email               = $_POST['email'];
  $id_level            = $_POST['id_level'];

  if (strlen($username) > 16) 
  {
    echo "
          <script type='text/javascript'>
          setTimeout(function() {
                                    swal({
                                        title: 'Oopss!!!',
                                        text: 'Username Tidak Boleh Lebih Dari 16 Karakter!',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.history.back();
                                  location.reload();
                                  },1000);</script>";
  }else{
    if ($password == $konfirmasi_password) 
    {
      $sql_get  = mysqli_query($conn, "SELECT * FROM tb_petugas WHERE username = '$username' ");
      $num_rows = mysqli_num_rows($sql_get);

      if ($num_rows == 0) 
      {
        $password       = md5($password);
        $konfirmasi_password = md5($konfirmasi_password);
        $insert = mysqli_query($conn, "INSERT INTO tb_petugas VALUES ('','$username', '$password', '$konfirmasi_password', '$nama_petugas','$email','$id_level')");
        if ($insert == TRUE) 
        {
          echo "
                <script type='text/javascript'>
                setTimeout(function() {
                                    swal({
                                        title: 'Berhasil!',
                                        text: 'Data Telah Ditambahkan',
                                        icon: 'success',
                                        type: 'success',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.location.replace('../data-petugas');
                                  },1000);</script>";
        }else{
          echo "
              <script type='text/javascript'>
              setTimeout(function() {
                                    swal({
                                        title: 'Gagal!!!',
                                        text: 'Data Gagal Ditambahkan',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.location.replace('../data-petugas');
                                  },1000);</script>";
        }

      }else{
          echo "
              <script type='text/javascript'>
              setTimeout(function() {
                                    swal({
                                        title: 'Oopss!!!',
                                        text: 'Username Sudah Ada',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.history.back();
                                  location.reload();
                                  },1000);</script>";
        }
    }else{
        echo "
              <script type='text/javascript'>
              setTimeout(function() {
                                    swal({
                                        title: 'Oopss!!!',
                                        text: 'Password Yang Anda Masukan Tidak Sama!',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.history.back();
                                  location.reload();
                                  },1000);</script>";
      }
  }
}else{
  echo "
        <script type='text/javascript'>
        setTimeout(function() {
                                    swal({
                                        title: 'Perhatian!!!',
                                        text: 'Anda Belum Mengisi Data',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.history.back();
                                  location.reload();
                                  },1000);</script>";
}

?>
</div>   
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
   
</body>
</html>