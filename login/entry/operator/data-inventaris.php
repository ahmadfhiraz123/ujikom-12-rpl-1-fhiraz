<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Operator</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="icon-Box-withFolders"></i> Data Inventaris</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="data-inventaris">Inventaris</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Data Inventaris</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="file_export-1" class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Kode Inventaris</th>
                                                <th>Nama Barang</th>
                                                <th>Jenis</th>
                                                <th>Kondisi</th>
                                                <th>Sumber</th>
                                                <th>Status</th>
                                                <th>Keterangan Stok</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                              include "../../koneksi.php";
                                              $no=1;
                                              $query = mysqli_query($conn, "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas ORDER BY id_inventaris DESC");
                                              while ($data=mysqli_fetch_array($query)) {
                                              ?>
                                            <tr class="text-center">
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $data['kode_inventaris']; ?></td>
                                                <td><?php echo $data['nama']; ?></td>
                                                <td><?php echo $data['nama_jenis']; ?></td>
                                                <td><?php echo $data['kondisi']; ?></td>
                                                <td><?php echo $data['sumber']; ?></td>
                                                <td>
                                                <?php if ($data['status'] == 'Tidak Aktif') {
                                                    echo "<label class='label label-danger'>Tidak Aktif</label>";
                                                    }else{
                                                    echo "<label class='label label-success'>Aktif</label>";
                                                    }   
                                                    ?> 
                                                </td>
                                                 <td>
                                                     <?php if ($data['jumlah'] >= 1) {?>
                                                     <label class="label label-success" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Tersedia">Stok Tersedia</label>
                                                     <?php }else{ ?>
                                                     <label class="label label-danger" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Kosong ">Stok Kosong</label>
                                                     <?php } ?>
                                                 </td>
                                                <td>
                                                <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                <?php }else{ ?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                                <div id="detail-inventaris<?php echo $data['id_inventaris']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Data Inventaris</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <form class="form-horizontal">
                                                            <div class="form-body">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kode Inventaris :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kode_inventaris']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Nama Barang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jenis :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_jenis']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Tanggal Register :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo date('d F Y', strtotime($data['tanggal_register'])); ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jumlah :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['jumlah']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Ruang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_ruang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!--/row-->
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Petugas :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_petugas']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kondisi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kondisi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Spesifikasi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['spesifikasi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Keterangan :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['keterangan_barang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Sumber :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['sumber']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                            <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static">
                                                                                    <label class="label label-table label-danger">Tidak Aktif</label></p>
                                                                                </div>
                                                                                <?php }else{ ?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <label class="label label-table label-success">Aktif</label></p>
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>  
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="../../dist/cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="../../dist/cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-1.init.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-advanced-2.init.js"></script>

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Menghapus?",   
                    text: "Kamu Akan Kehilangan Data Barang Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Hapus Data!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.status-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Yakin Ingin Merubah Status?",   
                    text: "Status Akan Berubah",   
                    type: "warning",   
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Rubah Status!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>
</html>