<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Operator</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/plugin/jquery-ui/jquery-ui.min.css" /> <!-- Load file css jquery-ui -->
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="sl-icon-folder-alt"></i> Laporan Peminjaman Barang</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="data-laporan-peminjaman">Laporan</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Laporan Peminjaman Barang</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="" method="GET" id="laporan">
                                    <div class="row">
                                    <div class="form-group col-md-6">
                                    <div class="col-md-12 m-b-20">
                                        <label for="recipient-name" class="control-label">Filter Berdasarkan :</label>
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="filter" id="filter">
                                                <option value="" disabled selected>Pilih</option>
                                                    <option value="1">Per Tanggal</option>
                                                    <option value="2">Per Bulan</option>
                                                    <option value="3">Per Tahun</option>
                                            </select>
                                    </div>
                                    <div class="col-md-12 m-b-20" id="form-tanggal">
                                        <label>Tanggal</label><br>
                                        <input type="text" name="tanggal" class="form-control" id="input-tanggal" />
                                    </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                    <div class="col-md-12 m-b-20" id="form-bulan">
                                        <label for="recipient-name" class="control-label">Bulan</label>
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="bulan">
                                                    <option value="" disabled selected>Pilih</option>
                                                    <option value="1">Januari</option>
                                                    <option value="2">Februari</option>
                                                    <option value="3">Maret</option>
                                                    <option value="4">April</option>
                                                    <option value="5">Mei</option>
                                                    <option value="6">Juni</option>
                                                    <option value="7">Juli</option>
                                                    <option value="8">Agustus</option>
                                                    <option value="9">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                            </select>
                                    </div>
                                    <div class="col-md-12 m-b-20" id="form-tahun">
                                        <label for="recipient-name" class="control-label">Tahun</label>
                                            <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="tahun">
                                                    <option value="" disabled selected>Pilih</option>
                                                     <?php
                                                        $query = "SELECT YEAR(tanggal_pinjam) AS tahun FROM tb_peminjaman GROUP BY YEAR(tanggal_pinjam)"; // Tampilkan tahun sesuai di tabel tb_peminjaman
                                                        $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query

                                                        while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                                            echo '<option value="'.$data['tahun'].'">'.$data['tahun'].'</option>';
                                                        }
                                                    ?>
                                            </select>
                                    </div>
                                    </div>
                                    </div>
                                <button type="submit" class="btn btn-dark waves-effect" style="background: #333d54;">Tampilkan</button>
                                <button class="btn btn-danger waves-effect"><a href="data-laporan-peminjaman" style="color: white;">Reset Filter</a></button>
                                </form>
                                <hr>
                                <?php
                                if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
                                    $filter = $_GET['filter']; // Ambil data filder yang dipilih user

                                    if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                                        $tgl = date('d F Y', strtotime($_GET['tanggal']));

                                        echo '<div class="alert alert-info"><b>Data Peminjaman Tanggal : '.$tgl.'</b></div>';
                                        echo '<button class="btn btn-success waves-effect"><a href="proses/print-laporan-peminjaman?filter=1&tanggal='.$_GET['tanggal'].'" style="color: white;">Cetak PDF</a></button><br /><br />';

                                        $query = "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam d ON p.id_detail_pinjam=d.id_detail_pinjam JOIN tb_inventaris i ON d.id_inventaris=i.id_inventaris JOIN tb_petugas g ON d.id_petugas=g.id_petugas JOIN tb_pegawai w ON p.id_pegawai=w.id_pegawai WHERE DATE(tanggal_pinjam)='".$_GET['tanggal']."' AND id_level='3'"; // Tampilkan data tb_peminjaman sesuai tanggal yang diinput oleh user pada filter
                                    }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                                            
                                        $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                                        echo '<div class="alert alert-info"><b>Data Peminjaman Bulan : '.$nama_bulan[$_GET['bulan']].' '.$_GET['tahun'].'</b></div>';
                                        echo '<button class="btn btn-success waves-effect"><a href="proses/print-laporan-peminjaman?filter=2&bulan='.$_GET['bulan'].'&tahun='.$_GET['tahun'].'" style="color: white;">Cetak PDF</a></button><br /><br />';

                                        $query = "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam d ON p.id_detail_pinjam=d.id_detail_pinjam JOIN tb_inventaris i ON d.id_inventaris=i.id_inventaris JOIN tb_petugas g ON d.id_petugas=g.id_petugas JOIN tb_pegawai w ON p.id_pegawai=w.id_pegawai WHERE MONTH(tanggal_pinjam)='".$_GET['bulan']."' AND YEAR(tanggal_pinjam)='".$_GET['tahun']."' AND id_level='3'"; // Tampilkan data tb_peminjaman sesuai bulan dan tahun yang diinput oleh user pada filter
                                    }else{ // Jika filter nya 3 (per tahun)
                                        
                                        echo '<div class="alert alert-info"><b>Data Peminjaman Tahun : '.$_GET['tahun'].'</b></div>';
                                        echo '<button class="btn btn-success waves-effect"><a href="proses/print-laporan-peminjaman?filter=3&tahun='.$_GET['tahun'].'" style="color: white;">Cetak PDF</a></button><br /><br />';
                                                       
                                        $query = "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam d ON p.id_detail_pinjam=d.id_detail_pinjam JOIN tb_inventaris i ON d.id_inventaris=i.id_inventaris JOIN tb_petugas g ON d.id_petugas=g.id_petugas JOIN tb_pegawai w ON p.id_pegawai=w.id_pegawai WHERE YEAR(tanggal_pinjam)='".$_GET['tahun']."' AND id_level='3'"; // Tampilkan data tb_peminjaman sesuai tahun yang diinput oleh user pada filter
                                    }
                                }else{ // Jika user tidak mengklik tombol tampilkan
                                    echo '<div class="alert alert-info"><b>Semua Data Peminjaman</b></div>';
                                    echo '<button class="btn btn-success waves-effect"><a href="proses/print-laporan-peminjaman" style="color: white;">Cetak PDF</a></button><br /><br />';

                                    $query = "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam d ON p.id_detail_pinjam=d.id_detail_pinjam JOIN tb_inventaris i ON d.id_inventaris=i.id_inventaris JOIN tb_petugas g ON d.id_petugas=g.id_petugas JOIN tb_pegawai w ON p.id_pegawai=w.id_pegawai WHERE id_level='3' ORDER BY tanggal_pinjam"; // Tampilkan semua data tb_peminjaman diurutkan berdasarkan tanggal
                                }
                                ?>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Tanggal</th>
                                                <th>Peminjam</th>
                                                <th>Barang</th>
                                                <th>Jumlah</th>
                                                <th>Tanggal Pinjam</th>
                                                <th>Tanggal Kembali</th>
                                                <th>Nama Pegawai</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
                                            $row = mysqli_num_rows($sql); // Ambil jumlah data dari hasil eksekusi $sql

                                            if($row > 0){ // Jika jumlah data lebih dari 0 (Berarti jika data ada)
                                                $no=1;
                                                while($data = mysqli_fetch_array($sql)){ // Ambil semua data dari hasil eksekusi $sql
                                                    $tgl = date('d F Y', strtotime($data['tanggal_pinjam'])); // Ubah format tanggal jadi dd-mm-yyyy

                                                    echo "<tr class='text-center'>";
                                                    echo "<td>".$no++."</td>";
                                                    echo "<td>".$tgl."</td>";
                                                    echo "<td>".$data['nama_petugas']."</td>";
                                                    echo "<td>".$data['nama']."</td>";
                                                    echo "<td>".$data['jumlah_barang']."</td>";
                                                    echo "<td>".date('d F Y, h:i:s', strtotime($data['tanggal_pinjam']))."</td>";
                                                    if ($data['status_peminjaman'] == 'Dipinjam') 
                                                    {
                                                    echo "<td>-</td>";
                                                    }else{
                                                    echo "<td>".date('d F Y, h:i:s', strtotime($data['tanggal_kembali']))."</td>";
                                                    }
                                                    echo "<td>".$data['nama_pegawai']."</td>";
                                                    echo "</tr>";
                                                }
                                            }else{ // Jika data tidak ada
                                                echo "<tr class='text-center'><td colspan='8'>Data tidak ada</td></tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>
    <script src="../../dist/js/jquery.validate.js"></script>
    <script src="../../assets/plugin/jquery-ui/jquery-ui.min.js"></script> <!-- Load file plugin js jquery-ui -->

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

    <script>
    $(document).ready(function(){ // Ketika halaman selesai di load
        $('#input-tanggal').datepicker({
            dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
        });

        $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

        $('#filter').change(function(){ // Ketika user memilih filter
            if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
                $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
                $('#form-tanggal').show(); // Tampilkan form tanggal
            }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
                $('#form-tanggal').hide(); // Sembunyikan form tanggal
                $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
            }else{ // Jika filternya 3 (per tahun)
                $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
                $('#form-tahun').show(); // Tampilkan form tahun
            }

            $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
        })
    })
    </script>

    <script>
    $().ready(function() {

        // validate signup form on keyup and submit
        $("#laporan").validate({
            rules: {
                filter: "required",
                tanggal: "required",
                bulan: "required",
                tahun: "required"
            },
            messages: {
                filter: "<label class='label label-danger'>Data Harus Diisi!</label>",
                tanggal: "<label class='label label-danger'>Data Harus Diisi!</label>",
                bulan: "<label class='label label-danger'>Data Harus Diisi!</label>",
                tahun: "<label class='label label-danger'>Data Harus Diisi!</label>"
            }
        });

    });
    </script>

</body>

</html>