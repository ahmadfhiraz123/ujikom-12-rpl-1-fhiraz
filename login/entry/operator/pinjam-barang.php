<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Operator</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <link href="../../assets/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="sl-icon-note"></i> Pinjam Barang</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Pinjam Barang</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="row">                           
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="proses/proses-barang-pinjam" method="post" class="form-horizontal form-material needs-validation" novalidate>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div class="col-md-12 m-b-20">
                                                        <label for="recipient-name" class="control-label">Barang :</label>
                                                        <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_inventaris" required="">
                                                            <option value="" disabled selected>Pilih Barang</option>
                                                            <?php
                                                            include "../../koneksi.php";
                                                            $query = mysqli_query($conn, "SELECT * from tb_inventaris WHERE jumlah>0 AND status='Aktif' ORDER BY id_inventaris");
                                                            while ($data=mysqli_fetch_array($query)) {
                                                            ?>
                                                            <option value="<?php echo $data['id_inventaris']; ?>"><?php echo $data['nama']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <label for="recipient-name" class="control-label">Jumlah :</label>
                                                        <input type="number" class="form-control" id="recipient-name1" name="jumlah_barang" maxlength="20" required="" min="1">
                                                        <p><i>* Minimal peminjaman 1 barang</i></p>
                                                    </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <label for="recipient-name" class="control-label">Peminjam :</label>
                                                        <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_petugas" required="">
                                                            <option value="" disabled selected>Nama Peminjam</option>
                                                            <?php
                                                            include "../../koneksi.php";
                                                            $query = mysqli_query($conn, "SELECT * from tb_petugas p INNER JOIN tb_level l ON p.id_level=l.id_level");
                                                            while ($data=mysqli_fetch_array($query)) {
                                                            ?>
                                                            <option value="<?php echo $data['id_petugas']; ?>"><?php echo $data['nama_petugas']; ?> &nbsp; || &nbsp; <?php echo $data['nama_level']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12 m-b-20">
                                                        <label for="recipient-name" class="control-label">Pegawai :</label>
                                                        <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="id_pegawai" required="">
                                                            <option value="" disabled selected>Nama Pegawai</option>
                                                            <?php
                                                            include "../../koneksi.php";
                                                            $query = mysqli_query($conn, "SELECT * from tb_pegawai");
                                                            while ($data=mysqli_fetch_array($query)) {
                                                            ?>
                                                            <option value="<?php echo $data['id_pegawai']; ?>"><?php echo $data['nama_pegawai']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>     
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-dark waves-effect" value="Action" name="action" style="background: #333d54;">Pinjam Barang</button>
                                                <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Reset</button>
                                                <button type="button" onclick="window.history.back(-1);"  class="btn btn-info waves-effect" data-dismiss="modal">Kembali</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <h5><i class="icon-Box-withFolders"></i> Data Inventaris</h5>
                                            <hr>
                                            <table id="zero_config" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th>No</th>
                                                        <th>Kode Inventaris</th>
                                                        <th>Nama Barang</th>
                                                        <th>Jumlah Barang</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                      include "../../koneksi.php";
                                                      $no=1;
                                                      $query1 = mysqli_query($conn, "SELECT * FROM tb_inventaris ORDER BY jumlah DESC");
                                                      while ($data1=mysqli_fetch_array($query1)) {
                                                      ?>
                                                    <tr class="text-center">
                                                        <td><?php echo $no++; ?></td>
                                                        <td><?php echo $data1['kode_inventaris']; ?></td>
                                                        <td><?php echo $data1['nama']; ?></td>
                                                        <td><?php echo $data1['jumlah']; ?></td>
                                                        <td>
                                                            <?php if ($data1['status'] == 'Aktif') {?>
                                                             <label class="label label-success" data-toggle="tooltip" data-placement="top" title="Aktif">Aktif</label>
                                                             <?php }else{ ?>
                                                             <label class="label label-danger" data-toggle="tooltip" data-placement="top" title="Tidak Aktif">Tidak Aktif</label>
                                                             <?php } ?>
                                                        </td>
                                                    </tr>      
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="../../assets/libs/magnific-popup/meg.init.js"></script>
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>

</html>