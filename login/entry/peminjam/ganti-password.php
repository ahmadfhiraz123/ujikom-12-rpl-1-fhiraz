<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Peminjam</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <link href="../../assets/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"><i class="ti-settings m-r-5 m-l-5"></i> Ganti Password</h4>
                        <div class="d-flex align-items-center">
                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Ganti Password</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- basic table -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                include '../../koneksi.php';
                                if (isset($_GET['id_petugas'])) {
                                    $id_petugas = $_GET['id_petugas'];
                                }
                                else {
                                die ("Error. No ID Selected! ");    
                                }
                                //proses ganti password
                                if (isset($_POST['Ganti'])) {
                                $id_petugas       = $_POST['id_petugas'];
                                $password_lama    = md5($_POST['password_lama']);
                                $password_baru    = md5($_POST['password_baru']);
                                $konf_password    = $_POST['konf_password'];
                                //cek old password
                                $query = "SELECT * FROM tb_petugas WHERE id_petugas='$id_petugas' AND password='$password_lama'";
                                $sql = mysqli_query ($conn, $query);
                                $hasil = mysqli_num_rows ($sql);
                                if (! $hasil >= 1) {
                                    ?>
                                        <script type='text/javascript'>
                                        setTimeout(function() {
                                                swal({
                                                    title: 'Password Lama Tidak Sesuai!',
                                                    text: 'Silahkan Ulang Kembali!',
                                                    icon: 'warning',
                                                    type: 'warning',
                                                    timer: 3000,
                                                    showConfirmButton: true
                                                });
                                                },10);
                                                    window.setTimeout(function(){
                                                    window.history.back();
                                                    location.reload();
                                                },3000);</script>
                                    <?php
                                        unset($_SESSION['id_petugas']);
                                        session_destroy();
                                }
                                //validasi data data kosong
                                else if (empty($_POST['password_baru']) || empty($_POST['konf_password'])) {
                                        echo "
                                            <script type='text/javascript'>
                                                setTimeout(function() {
                                                    swal({
                                                        title: 'Ganti Password Gagal!!!',
                                                        text: 'Data Tidak Boleh Kosong!',
                                                        icon: 'warning',
                                                        type: 'warning',
                                                        timer: 3000,
                                                        showConfirmButton: true
                                                      });
                                                  },10);
                                                  window.setTimeout(function(){
                                                  window.history.back();
                                                  location.reload();
                                                  },2000);</script>";      
                                }
                                    //validasi input konfirm password
                                else if (($_POST['password_baru']) != ($_POST['konf_password'])) {
                                        echo "
                                            <script type='text/javascript'>
                                                setTimeout(function() {
                                                    swal({
                                                        title: 'Ganti Password Gagal!!!',
                                                        text: 'Password Baru Dengan Konfirmasi Password Harus Sama!',
                                                        icon: 'warning',
                                                        type: 'warning',
                                                        timer: 3000,
                                                        showConfirmButton: true
                                                      });
                                                  },10);
                                                  window.setTimeout(function(){
                                                  window.history.back();
                                                  location.reload();
                                                  },2000);</script>";      
                                }
                                else {
                                //update data
                                $query = "UPDATE tb_petugas SET password='$password_baru', konfirmasi_password='$password_baru' WHERE id_petugas='$id_petugas'";
                                $sql = mysqli_query ($conn, $query);
                                //setelah berhasil update
                                if ($sql) {
                                    echo "
                                            <script type='text/javascript'>
                                                setTimeout(function() {
                                                    swal({
                                                        title: 'Ganti Password Berhasil!',
                                                        text: 'Silahkan Login Kembali',
                                                        icon: 'success',
                                                        type: 'success',
                                                        timer: 3000,
                                                        showConfirmButton: true
                                                      });
                                                  },10);
                                                  window.setTimeout(function(){
                                                  window.location.replace('logout');
                                                  },3000);</script>";  
                                } else {
                                    echo "
                                            <script type='text/javascript'>
                                                setTimeout(function() {
                                                    swal({
                                                        title: 'Ganti Password Gagal!!!',
                                                        text: 'Silahkan Ulang Kembali !!!',
                                                        icon: 'warning',
                                                        type: 'warning',
                                                        timer: 3000,
                                                        showConfirmButton: true
                                                      });
                                                  },10);
                                                  window.setTimeout(function(){
                                                  window.history.back();
                                                  location.reload();
                                                  },2000);</script>";      
                                }
                                }
                                }
                            ?>

                            <div class="row">
                                <?php 
                                    include "../../koneksi.php";
                                    $id_petugas = $_GET['id_petugas'];
                                    $query = mysqli_query($conn, "SELECT * FROM tb_petugas p INNER JOIN tb_level l ON p.id_level=l.id_level WHERE id_petugas='$id_petugas'");
                                    while ($data = mysqli_fetch_array($query)) {
                                ?>
                                <div class="col-md-3">
                                    <div class="row el-element-overlay">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="el-card-item">
                                                    <div class="el-card-avatar el-overlay-1"> <img src="../../assets/images/icon/1.png" alt="user" />
                                                        <div class="el-overlay">
                                                            <ul class="list-style-none el-info">
                                                                <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="../../assets/images/icon/1.png"><i class="sl-icon-magnifier"></i></a></li>
                                                                <li class="el-item"><button class="btn default btn-outline el-link" data-toggle="modal" data-target="#detail-petugas<?php echo $data['id_petugas'];?>" ><i class="sl-icon-eye"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="el-card-content">
                                                        <h4 class="m-b-0"><?php echo $data['nama_petugas']; ?></h4> <span class="text-muted"><?php echo $data['nama_level']; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="detail-petugas<?php echo $data['id_petugas']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Petugas</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <form class="form-horizontal">
                                                            <div class="form-body">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-5">Nama Petugas :</label>
                                                                                <div class="col-md-7">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_petugas']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-5">Username :</label>
                                                                                <div class="col-md-7">
                                                                                    <p class="form-control-static"> <?php echo $data['username']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-5">Email :</label>
                                                                                <div class="col-md-7">
                                                                                    <p class="form-control-static"> <?php echo $data['email']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-5">Level :</label>
                                                                                <div class="col-md-7">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_level']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>  
                                <?php } ?>
                                <div class="col-md-9">
                                    <form action="" method="post" class="form-horizontal form-material needs-validation" novalidate>
                                        <div class="form-group col-md-12">
                                            <input name="id_petugas" type="hidden" class="form-control" value="<?=$id_petugas?>">
                                            <div class="col-md-12 m-b-20">
                                                <label for="recipient-name" class="control-label">Password Lama :</label>
                                                <input type="password" class="form-control" id="recipient-name1" name="password_lama" minlength="8" maxlength="16" required="">
                                            </div>
                                            <div class="col-md-12 m-b-20">
                                                <label for="recipient-name" class="control-label">Password Baru :</label>
                                                <input type="password" class="form-control" id="password1" name="password_baru" minlength="8" maxlength="16" required="">
                                                <small id="pass-info" class="help-block"></small>
                                            </div>
                                            <div class="col-md-12 m-b-20">
                                                <label for="recipient-name" class="control-label">Konfirmasi Password :</label>
                                                <input type="password" class="form-control" id="password2" name="konf_password" minlength="8" maxlength="16" required="">
                                                <small id="pass-info" class="help-block"></small>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-dark waves-effect" value="Ganti" name="Ganti" style="background: #333d54;">Ganti Password</button>
                                            <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">Reset</button>
                                            <button type="submit" onclick="window.history.back(-1);" class="btn btn-info waves-effect" data-dismiss="modal">Kembali</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="../../assets/libs/magnific-popup/meg.init.js"></script>
    <script src="../../assets/libs/password-strenght/pw_strenght_1.js"></script>

    <script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    </script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>

</html>