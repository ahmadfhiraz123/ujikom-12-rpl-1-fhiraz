<?php
include('cek.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Peminjam</title>
    <!-- Custom CSS -->
    <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="../../assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="../../assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <link href="../../assets/libs/sweetalert2/sweetalert.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php include 'page/header.php'; ?>
        <?php include 'page/sidebar-left.php'; ?>
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="index">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Welcome back  -->
                <!-- ============================================================== -->
                <?php
                    include '../../koneksi.php';
                    $username = $_SESSION['username'];
                    $ambildata = mysqli_query($conn, "SELECT * FROM tb_petugas WHERE username = '$username' ");
                    while ($data_user = mysqli_fetch_array($ambildata)) 
                    {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card  bg-light no-card-border">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div class="m-r-10">
                                        <img src="../../assets/images/icon/1.1.png" alt="user" width="60" class="rounded-circle" />
                                    </div>
                                    <div>
                                        <h3 class="m-b-0">Welcome, <b style="color: #3587d8;"><?php echo $data_user['nama_petugas']; ?>!</b></h3>
                                        <span>
                                            <script type='text/javascript'>
                                            <!--
                                            var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                                            var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
                                            var date = new Date();
                                            var day = date.getDate();
                                            var month = date.getMonth();
                                            var thisDay = date.getDay(),
                                                thisDay = myDays[thisDay];
                                            var yy = date.getYear();
                                            var year = (yy < 1000) ? yy + 1900 : yy;
                                            document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
                                            //-->
                                            </script>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
                        <div class="row">
                            <!-- Column -->
                            <div class="col-sm-12 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-success"><i class="fas fa-hourglass-half"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h4 class="m-b-0">Total Peminjaman Barang</h4>
                                                <span class="text-muted">Status Dipinjam</span>
                                            </div>
                                            <div class="ml-auto align-self-center">
                                                <h2 class="font-medium m-b-0">
                                                    <?php
                                                    include "../../koneksi.php";
                                                    $nama_petugas = $_SESSION['username'];
                                                    $absen = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam dt ON p.id_detail_pinjam=dt.id_detail_pinjam JOIN tb_petugas g ON dt.id_petugas=g.id_petugas WHERE status_peminjaman='Dipinjam' AND username='$nama_petugas'"));
                                                    echo $absen;
                                                    ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Column -->
                            <!-- Column -->
                            <div class="col-sm-12 col-md-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                            <div class="round align-self-center round-info"><i class="ti-check"></i></div>
                                            <div class="m-l-10 align-self-center">
                                                <h4 class="m-b-0">Total Peminjaman Barang</h4>
                                                <span class="text-muted">Status Dikembalikan</span>
                                            </div>
                                            <div class="ml-auto align-self-center">
                                                <h2 class="font-medium m-b-0">
                                                    <?php
                                                    include "../../koneksi.php";
                                                    $nama_petugas = $_SESSION['username'];
                                                    $absen = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM tb_peminjaman p INNER JOIN tb_detail_pinjam dt ON p.id_detail_pinjam=dt.id_detail_pinjam JOIN tb_petugas g ON dt.id_petugas=g.id_petugas WHERE status_peminjaman='Dikembalikan' AND username='$nama_petugas'"));
                                                    echo $absen;
                                                    ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                            <div class="card">
                            <div class="card-body">
                            <h5 class="card-subtitle">
                                Data Inventaris
                            </h5>
                            <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr class="text-center">
                                                <th>No</th>
                                                <th>Kode Inventaris</th>
                                                <th>Nama Barang</th>
                                                <th>Jenis</th>
                                                <th>Kondisi</th>
                                                <th>Keterangan Stok</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                              include "../../koneksi.php";
                                              $no=1;
                                              $query = mysqli_query($conn, "SELECT * FROM tb_inventaris i INNER JOIN tb_jenis j ON i.id_jenis=j.id_jenis JOIN tb_ruang r ON i.id_ruang=r.id_ruang JOIN tb_petugas p ON i.id_petugas=p.id_petugas ORDER BY id_inventaris DESC");
                                              while ($data=mysqli_fetch_array($query)) {
                                              ?>
                                            <tr class="text-center">
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $data['kode_inventaris']; ?></td>
                                                <td><?php echo $data['nama']; ?></td>
                                                <td><?php echo $data['nama_jenis']; ?></td>
                                                <td><?php echo $data['kondisi']; ?></td>
                                                 <td>
                                                     <?php if ($data['jumlah'] > 1) {?>
                                                     <label class="label label-success" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Tersedia">Stok Tersedia</label>
                                                     <?php }else{ ?>
                                                     <label class="label label-danger" data-toggle="tooltip" data-placement="top" title="Jumlah Barang Kosong ">Stok Kosong</label>
                                                     <?php } ?>
                                                 </td>
                                                <td>
                                                <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                <?php }else{ ?>
                                                    <button type="button" class="btn waves-effect waves-light btn-cyan card-hover" data-toggle="modal" data-target="#detail-inventaris<?php echo $data['id_inventaris']; ?>"><i class="fa fa-eye"></i></button>
                                                <?php } ?>
                                                </td>
                                            </tr>
                                                <div id="detail-inventaris<?php echo $data['id_inventaris']; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Data Inventaris</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <form class="form-horizontal">
                                                            <div class="form-body">
                                                                <div class="card-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kode Inventaris :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kode_inventaris']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Nama Barang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jenis :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_jenis']; ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Tanggal Register :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo date('d F Y', strtotime($data['tanggal_register'])); ?> </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Jumlah :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['jumlah']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Ruang :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_ruang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!--/row-->
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Petugas :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['nama_petugas']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Kondisi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['kondisi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Spesifikasi :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['spesifikasi']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Keterangan :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['keterangan_barang']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                                <label class="control-label text-right col-md-6">Sumber :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <?php echo $data['sumber']; ?>  </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="form-group row">
                                                                            <?php if ($data['status'] == 'Tidak Aktif') {?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static">
                                                                                    <label class="label label-table label-danger">Tidak Aktif</label></p>
                                                                                </div>
                                                                                <?php }else{ ?>
                                                                                <label class="control-label text-right col-md-6">Status :</label>
                                                                                <div class="col-md-6">
                                                                                    <p class="form-control-static"> <label class="label label-table label-success">Aktif</label></p>
                                                                                </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>  
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by ISP Skanic 2019. Designed and Developed by
                <a href="https://bungdesain.blogspot.com/">Ahmad Fhiraz</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <?php include 'page/sidebar-right.php'; ?>
    <div class="chat-windows"></div>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../../assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- apps -->
    <script src="../../dist/js/app.min.js"></script>
    <script src="../../dist/js/app.init.horizontal.js"></script>
    <script src="../../dist/js/app-style-switcher.horizontal.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../../assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="../../assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="../../dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../../dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../../dist/js/custom.min.js"></script>
    <script src="../../assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="../../assets/libs/sweetalert2/sweet-alert.init.js"></script>
    <script src="../../assets/libs/sweetalert2/sweetalert.min.js"></script>
    <!--This page plugins -->
    <script src="../../assets/extra-libs/DataTables/datatables.min.js"></script>
    <script src="../../dist/js/pages/datatable/datatable-basic.init.js"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="../../assets/libs/chartist/dist/chartist.min.js"></script>
    <script src="../../assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js"></script>
    <!--c3 charts -->
    <script src="../../assets/extra-libs/c3/d3.min.js"></script>
    <script src="../../assets/extra-libs/c3/c3.min.js"></script>
    <!--chartjs -->
    <script src="../../assets/libs/raphael/raphael.min.js"></script>
    <script src="../../assets/libs/morris.js/morris.min.js"></script>

    <script src="../../dist/js/pages/dashboards/dashboard1.js"></script>

    <script>
        jQuery(document).ready(function($){
            $('.logout-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                    title: "Apakah Kamu Yakin Ingin Logout?",   
                    text: "Kamu Akan Keluar Dari Website Ini",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonClass: "btn-danger",
                    cancelButtonClass: "btn-dark",      
                    confirmButtonText: "Ya, Logout!",     
                    closeOnConfirm: false
                },function(){
                    window.location.href = getLink
                });
                return false;
            });
        });
    </script>

</body>

</html>