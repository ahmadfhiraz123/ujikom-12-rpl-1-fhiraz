<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>ISP-SKANIC 2019 | Administrator</title>
    <!-- Custom CSS -->
    <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="assets/libs/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url(assets/images/big/auth-bg.jpg) no-repeat center center;">

<?php
session_start();
include 'koneksi.php';

//fungsi menghindari injeksi dari user yang jahil
function anti_injection($data)
{
	$filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	return $filter;
}

if (isset($_POST['login'])) 
{
	$username = anti_injection(addslashes(trim($_POST['username'])));
	$password	= anti_injection(md5($_POST['password']));

	$query = mysqli_query($conn, "SELECT * FROM tb_petugas WHERE username='$username' AND password='$password' ");
	if (mysqli_num_rows($query) == 0) 
	{
		echo "
		<script type='text/javascript'>
		setTimeout(function() {
	                                    swal({
	                                        title: 'Oopss!!!',
	                                        text: 'Username Atau Password Anda Salah!',
	                                        icon: 'warning',
	                                        type: 'warning',
	                                        timer: 3000,
	                                        showConfirmButton: true
	                                      });
	                                  },10);
	                                  window.setTimeout(function(){
	                                  window.location.replace('index');
		                              },1000);</script>";
	}else{
		$row = mysqli_fetch_assoc($query);
		$_SESSION['username']	= $row['username'];
		$_SESSION['id_level']  = $row['id_level'];
		
		if($row['id_level'] == "1")
		{	
			echo "
			<script type='text/javascript'>
			setTimeout(function() {
		                                    swal({
		                                        title: 'Login Berhasil !!!',
		                                        text: 'Anda Masuk Sebagai Administrator!',
		                                        icon: 'success',
		                                        type: 'success',
		                                        timer: 3000,
		                                        showConfirmButton: true
		                                      });
		                                  },10);
		                                  window.setTimeout(function(){
		                                  window.location.replace('entry/admin/');
		                                  },1000);</script>";
		}
		else if($row['id_level'] =="2")
		{
			echo "
			<script type='text/javascript'>
			setTimeout(function() {
		                                    swal({
		                                        title: 'Login Berhasil !!!',
		                                        text: 'Anda Masuk Sebagai Operator!',
		                                        icon: 'success',
		                                        type: 'success',
		                                        timer: 3000,
		                                        showConfirmButton: true
		                                      });
		                                  },10);
		                                  window.setTimeout(function(){
		                                  window.location.replace('entry/operator/');
		                                  },1000);</script>";
		}
		else if($row['id_level'] =="3")
		{
			echo "
			<script type='text/javascript'>
			setTimeout(function() {
		                                    swal({
		                                        title: 'Login Berhasil !!!',
		                                        text: 'Anda Masuk Sebagai Peminjam!',
		                                        icon: 'success',
		                                        type: 'success',
		                                        timer: 3000,
		                                        showConfirmButton: true
		                                      });
		                                  },10);
		                                  window.setTimeout(function(){
		                                  window.location.replace('entry/peminjam/');
		                                  },1000);</script>";
		}
		else
		{
			echo "
			<script type='text/javascript'>
			setTimeout(function() {
		                                    swal({
		                                        title: 'Login Gagal !!!',
		                                        text: 'Anda Tidak Bisa Login!',
		                                        icon: 'warning',
		                                        type: 'warning',
		                                        timer: 3000,
		                                        showConfirmButton: true
		                                      });
		                                  },10);
		                                  window.setTimeout(function(){
		                                  window.location.replace('index');
		                              	  },1000);</script>";
		}
	}
}else{
	echo "
	<script type='text/javascript'>
	setTimeout(function() {
                                    swal({
                                        title: 'Oopss!!!',
                                        text: 'Anda Belum Mengisi Form!',
                                        icon: 'warning',
                                        type: 'warning',
                                        timer: 3000,
                                        showConfirmButton: true
                                      });
                                  },10);
                                  window.setTimeout(function(){
                                  window.location.replace('index');
		                          },1000);</script>";
}
?>

</div>   
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/libs/sweetalert2/dist/sweetalert2.all.min.js"></script>
    <script src="assets/libs/sweetalert2/sweet-alert.init.js"></script>
   
</body>
</html>